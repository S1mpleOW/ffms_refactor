package s1mple.dlowji.ffms_refactor.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import s1mple.dlowji.ffms_refactor.entities.BookedTicket;
import s1mple.dlowji.ffms_refactor.services.IBookedTicketService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class BookedTicketController {
    private final IBookedTicketService bookedTicketService;

    public BookedTicketController(IBookedTicketService bookedTicketService) {
        this.bookedTicketService = bookedTicketService;
    }

    @GetMapping("/booked-price/{year}/{quarter}")
    public ResponseEntity<?> getTotalBookedPriceByQuarter(@PathVariable("year") int year, @PathVariable("quarter") int quarter) {

        List<BookedTicket> bookedTicketList = bookedTicketService.getBookedTicketByQuarter(quarter, year);

        int totalPrice = 0;

        for (BookedTicket receipt:bookedTicketList) {
            totalPrice += receipt.getTotalPrice();
        }

        Map<String, Object> response = new HashMap<>();
        response.put("status", HttpStatus.OK.value());
        response.put("total_price", totalPrice);
        response.put("year", year);
        response.put("quarter", quarter);

        return ResponseEntity.ok(response);
    }
}
