package s1mple.dlowji.ffms_refactor.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import s1mple.dlowji.ffms_refactor.dto.request.ImportItemsForm;
import s1mple.dlowji.ffms_refactor.dto.request.OrderMoreProductDTO;
import s1mple.dlowji.ffms_refactor.dto.response.ResponseMessage;
import s1mple.dlowji.ffms_refactor.entities.*;
import s1mple.dlowji.ffms_refactor.entities.enums.EquipmentStatus;
import s1mple.dlowji.ffms_refactor.entities.enums.PaymentStatus;
import s1mple.dlowji.ffms_refactor.repositories.*;
import s1mple.dlowji.ffms_refactor.security.userprincipal.UserPrincipal;
import s1mple.dlowji.ffms_refactor.services.ItemService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class ImportController {

	private final ImportRepository importRepository;

	private final ItemService itemService;

	private final SupplierRepository supplierRepository;

	private final EmployeeRepository employeeRepository;

	private final ItemRepository itemRepository;

	private final ImportDetailRepository importDetailRepository;

	public ImportController(ImportRepository importRepository, ItemService itemService, SupplierRepository supplierRepository, EmployeeRepository employeeRepository, ItemRepository itemRepository, ImportDetailRepository importDetailRepository) {
		this.importRepository = importRepository;
		this.itemService = itemService;
		this.supplierRepository = supplierRepository;
		this.employeeRepository = employeeRepository;
		this.itemRepository = itemRepository;
		this.importDetailRepository = importDetailRepository;
	}
    /*
		Input {
			id supplier
			image
			import price
			category
			name
			note
			quantity
			status
			unit
			delivery date
		}
	 */

	@PostMapping("/item/order-more")
	public ResponseEntity<?> orderMore(@Valid @RequestBody OrderMoreProductDTO orderMoreProductDTO) {
		if (!itemService.existsById((orderMoreProductDTO.getItemID()))) {
			return new ResponseEntity<>(new ResponseMessage("The supplier is not " +
			"existed", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
		}
		Item item = itemService.findById(orderMoreProductDTO.getItemID());
		int importPrice = item.getImportPrice();
		int moreQuantity = orderMoreProductDTO.getQuantity();
		item.setQuantity(moreQuantity + item.getQuantity());
		if (item.getStatus().equals(EquipmentStatus.SOLD_OUT)) {
			item.setStatus(EquipmentStatus.AVAILABLE);
		}

		Authentication authentication =
		SecurityContextHolder.getContext().getAuthentication();
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
		Long currentAccountId = userPrincipal.getId();

		Employee employee = null;

		if (employeeRepository.findEmployeeByAccount_Id(currentAccountId).isPresent()) {
			employee = employeeRepository.findEmployeeByAccount_Id(currentAccountId).get();
		}

		int totalPrice = importPrice * moreQuantity;
		ImportReceipt receipt =
		ImportReceipt.builder()
		.note(orderMoreProductDTO.getNote())
		.paymentStatus(PaymentStatus.PROCESSING)
		.totalPrice(totalPrice)
		.employee(employee)
		.build();

		//build import receipt detail
		ImportReceiptDetail receiptDetail =
		ImportReceiptDetail.builder()
		.id(ImportReceiptDetailKey.builder().importReceiptId(receipt.getId()).itemId(item.getId()).build())
		.importReceipt(receipt)
		.item(item)
		.deliveryDate(orderMoreProductDTO.getDelivery_date())
		.quantity(orderMoreProductDTO.getQuantity())
		.build();


		itemRepository.save(item);
		ImportReceipt receiptBody = importRepository.save(receipt);
		importDetailRepository.save(receiptDetail);

		Map<String, Object> response = new HashMap<>();
		response.put("status", HttpStatus.OK.value());
		response.put("item_id", item.getId());
		response.put("receipt_id", receiptBody.getId());
		response.put("total_price", totalPrice);
		response.put("payment_status", PaymentStatus.PROCESSING.toString());
		response.put("created_at", receiptBody.getCreatedAt());
		return ResponseEntity.ok(response);
	}

	@PostMapping("/item/order")
	public ResponseEntity<?> importItems(@Valid @RequestBody ImportItemsForm importItemsForm) {
		if (!supplierRepository.existsById((importItemsForm.getSupplier_id()))) {
			return new ResponseEntity<>(new ResponseMessage("The supplier is not " +
			"existed", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
		}

		int importPrice = importItemsForm.getImport_price();
		int sellPrice = importItemsForm.getSell_price();
		int quantity = importItemsForm.getQuantity();
		int totalPrice = importPrice * quantity;

		if (importPrice > sellPrice) {
			return new ResponseEntity<>(new ResponseMessage("The sell price must be" +
			" greater or equal than import price", HttpStatus.BAD_REQUEST.value()),
			HttpStatus.BAD_REQUEST);
		}

		Authentication authentication =
		SecurityContextHolder.getContext().getAuthentication();
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
		Long currentAccountId = userPrincipal.getId();

		Employee employee = null;

		if (employeeRepository.findEmployeeByAccount_Id(currentAccountId).isPresent()) {
			employee = employeeRepository.findEmployeeByAccount_Id(currentAccountId).get();
		}
		Supplier supplier = supplierRepository.findById(importItemsForm.getSupplier_id()).get();

		Item item;

		if (itemService.existsByNameIgnoreCase((importItemsForm.getName()))) {
			item = itemService.findByName(importItemsForm.getName());
			item.setQuantity(item.getQuantity() + importItemsForm.getQuantity());
		} else {
			//build item
			item =
			Item.builder()
			.name(importItemsForm.getName())
			.status(EquipmentStatus.AVAILABLE)
			.importPrice(importPrice)
			.sellPrice(sellPrice)
			.unit(importItemsForm.getUnit())
			.image(importItemsForm.getImage())
			.quantity(quantity)
			.note(importItemsForm.getNote())
			.itemCategory(importItemsForm.getItem_category())
			.supplier(supplier)
			.build();
		}
		//build import receipt
		ImportReceipt receipt = ImportReceipt.builder()
		.note(importItemsForm.getNote())
		.paymentStatus(PaymentStatus.PROCESSING)
		.totalPrice(totalPrice)
		.employee(employee)
		.build();


		//build import receipt detail
		ImportReceiptDetail receiptDetail =
		ImportReceiptDetail.builder()
		.id(ImportReceiptDetailKey.builder().importReceiptId(receipt.getId()).itemId(item.getId()).build())
		.importReceipt(receipt)
		.item(item)
		.deliveryDate(importItemsForm.getDelivery_date())
		.quantity(importItemsForm.getQuantity())
		.build();

		itemRepository.save(item);
		ImportReceipt receiptBody = importRepository.save(receipt);
		importDetailRepository.save(receiptDetail);
		System.out.println(item);
		System.out.println(receiptBody);
		System.out.println(receiptDetail.getItem());

		Map<String, Object> response = new HashMap<>();
		response.put("status", HttpStatus.OK.value());
		response.put("item_id", item.getId());
		response.put("receipt_id", receiptBody.getId());
		response.put("total_price", totalPrice);
		response.put("payment_status", PaymentStatus.PROCESSING.toString());
		response.put("created_at", receiptBody.getCreatedAt());
		return ResponseEntity.ok(response);
	}
}
