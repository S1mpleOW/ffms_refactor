package s1mple.dlowji.ffms_refactor.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import s1mple.dlowji.ffms_refactor.entities.*;
import s1mple.dlowji.ffms_refactor.repositories.BookedTicketDetailRepository;
import s1mple.dlowji.ffms_refactor.repositories.BookedTicketRepository;
import s1mple.dlowji.ffms_refactor.services.IBookedTicketService;
import s1mple.dlowji.ffms_refactor.services.ICustomerService;
import s1mple.dlowji.ffms_refactor.services.IEmployeeServices;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IBookedTicketServiceImpl implements IBookedTicketService {
	@Autowired
	private BookedTicketRepository bookedTicketRepository;

	@Autowired
	private BookedTicketDetailRepository bookedTicketDetailRepository;

	@Autowired
	private ICustomerService customerService;

	@Autowired
	private IEmployeeServices employeeServices;

	@Override
	public BookedTicket save(BookedTicket bookedTicket) {
		return bookedTicketRepository.save(bookedTicket);
	}

	@Override
	public BookedTicketDetail save(BookedTicketDetail bookedTicketDetail) {
		return bookedTicketDetailRepository.save(bookedTicketDetail);
	}

	@Override
	public List<BookedTicketDetail> getPlayedBookedTicketDetailByDate(Long accountId, LocalDateTime datetimeNow) {
		List<BookedTicketDetail> bookedTicketDetails =
		bookedTicketDetailRepository.findAll();
		List<BookedTicketDetail> bookedTicketDetailList = new ArrayList<>();
		System.out.println(datetimeNow);
		for(BookedTicketDetail btd:bookedTicketDetails) {
			System.out.println(btd);
			if (btd.getEndTime().toLocalDate().isBefore(datetimeNow.toLocalDate())) {
				BookedTicket bookedTicket = btd.getBookedTicket();
				Customer customer = bookedTicket.getCustomer();
				if (customer != null) {
					if (customer.getAccount().getId() == accountId) {
						bookedTicketDetailList.add(btd);
					}
				}
			}
		}
		return bookedTicketDetailList;
	}

	@Override
	public List<BookedTicketDetail> getPlayingBookedTicketDetailByDate(LocalDateTime dateTime) {
		List<BookedTicketDetail> bookedTicketDetailList = new ArrayList<>();

		for (BookedTicketDetail btd:bookedTicketDetailRepository.findAll()) {
			LocalDate date = btd.getStartTime().toLocalDate();
			LocalDate checkDate = dateTime.toLocalDate();

			if (!date.isEqual(checkDate)) {
				continue;
			}

			if (btd.getStartTime().isBefore(dateTime) && btd.getEndTime().isAfter(dateTime)) {
				bookedTicketDetailList.add(btd);
			}
		}

		return bookedTicketDetailList;
	}

	@Override
	public List<BookedTicketDetail> getWillPlayBookedTicketDetailByDate(Long accountId,
																																			LocalDateTime datetimeNow) {
		List<BookedTicketDetail> bookedTicketDetails = bookedTicketDetailRepository.findAll();
		List<BookedTicketDetail> bookedTicketDetailList = new ArrayList<>();
		System.out.println(datetimeNow);
		for(BookedTicketDetail btd:bookedTicketDetails) {
			System.out.println(btd);
			if (btd.getStartTime().toLocalDate().isAfter(datetimeNow.toLocalDate())) {
				BookedTicket bookedTicket = btd.getBookedTicket();
				Customer customer = bookedTicket.getCustomer();
				if (customer != null) {
					if (customer.getAccount().getId() == accountId) {
						bookedTicketDetailList.add(btd);
					}
				}
			}
		}
		return bookedTicketDetailList;
	}

	@Override
	public List<BookedTicketDetail> getBookedTicketDetailByWeek(LocalDateTime firstDay, LocalDateTime lastDay, Long field_id) {
		List<BookedTicketDetail> bookedTicketDetailList = new ArrayList<>();

		for (BookedTicketDetail btd : bookedTicketDetailRepository.findAll()) {
			if (btd.getFootballField().getId() == field_id && btd.getStartTime().isAfter(firstDay) && btd.getStartTime().isBefore(lastDay) && !btd.isCanceled()) {
				bookedTicketDetailList.add(btd);
			}
		}
		return bookedTicketDetailList;
	}

	@Override
	public List<BookedTicket> getBookedTicketByQuarter(int year, int quarter) {
		List<BookedTicket> bookedTicketList = new ArrayList<>();

		for (BookedTicket receipt: bookedTicketRepository.findAll()) {
			if (receipt.getCreatedAt().getYear() == year && receipt.getCreatedAt().get(IsoFields.QUARTER_OF_YEAR) == quarter) {
				bookedTicketList.add(receipt);
			}
		}

		return bookedTicketList;
	}

	@Override
	public List<BookedTicket> findBookedTicketByAccountId(Long accountId) {
		List<BookedTicket> bookedTickets = new ArrayList<>();
		Optional<Customer> customer = customerService.findCustomerByAccountId(accountId);
		if(customer.isPresent() && !customer.isEmpty()) {
			bookedTickets = bookedTicketRepository.findBookedTicketsByCustomer_Id(customer.get().getId());
		}

		return bookedTickets;
	}

	@Override
	public int getBookedPriceByMonth(int month, int year) {
		List<BookedTicket> bookedTicketList = new ArrayList<>();

		for (BookedTicket receipt: bookedTicketRepository.findAll()) {
			if (receipt.getCreatedAt().getYear() == year && receipt.getCreatedAt().getMonthValue() == month) {
				bookedTicketList.add(receipt);
			}
		}

		int totalPrice = 0;

		for (BookedTicket receipt:bookedTicketList) {
			boolean isNotCancel =
			receipt.getBookedTicketDetails()
			.stream().filter(bookedTicketDetail -> bookedTicketDetail.isCanceled())
			.collect(Collectors.toList()).isEmpty();
			if(isNotCancel) {
				totalPrice += receipt.getTotalPrice();
			}
		}

		return totalPrice;
	}

	@Override
	public int getBookedPriceByDay(int day, int month, int year) {
		List<BookedTicket> bookedTicketList = new ArrayList<>();

		for (BookedTicket receipt: bookedTicketRepository.findAll()) {
			if (receipt.getCreatedAt().getYear() == year && receipt.getCreatedAt().getMonthValue() == month && receipt.getCreatedAt().getDayOfMonth() == day) {
				bookedTicketList.add(receipt);
			}
		}

		int totalPrice = 0;

		for (BookedTicket receipt:bookedTicketList) {
			totalPrice += receipt.getTotalPrice();
		}

		return totalPrice;
	}
}
