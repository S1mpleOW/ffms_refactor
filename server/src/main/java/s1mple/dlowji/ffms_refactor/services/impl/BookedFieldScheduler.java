package s1mple.dlowji.ffms_refactor.services.impl;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import s1mple.dlowji.ffms_refactor.entities.Account;
import s1mple.dlowji.ffms_refactor.entities.BookedTicket;
import s1mple.dlowji.ffms_refactor.entities.BookedTicketDetail;
import s1mple.dlowji.ffms_refactor.entities.Customer;
import s1mple.dlowji.ffms_refactor.repositories.BookedTicketRepository;
import s1mple.dlowji.ffms_refactor.services.EmailService;

import javax.mail.MessagingException;
import java.time.ZonedDateTime;
import java.util.List;

@Component
public class BookedFieldScheduler {
    private final BookedTicketRepository bookedTicketRepository;

    private final EmailService emailService;

    public BookedFieldScheduler(BookedTicketRepository bookedTicketRepository, EmailService emailService) {
        this.bookedTicketRepository = bookedTicketRepository;
        this.emailService = emailService;
    }

    @Scheduled(cron = "0 0 5 * * *")
    public void checkBookingAndSendReminder() throws MessagingException {
        System.out.println("send mail every day");
        ZonedDateTime today = ZonedDateTime.now();
        List<BookedTicket> bookedTicketList = bookedTicketRepository.findAll();

        for (BookedTicket bookedTicket : bookedTicketList) {
            List<BookedTicketDetail> bookedTicketDetails = bookedTicket.getBookedTicketDetails();
            for (BookedTicketDetail bookedTicketDetail : bookedTicketDetails) {
                ZonedDateTime orderDate = bookedTicketDetail.getOrderDate();
                if (!orderDate.toLocalDate().isEqual(today.toLocalDate())) {
                    bookedTicketList.remove(bookedTicket);
                }
            }
        }
        for (BookedTicket bookedTicket : bookedTicketList) {
            Customer customer = bookedTicket.getCustomer();
            Account customerAcc = customer.getAccount();
            bookedTicket.getBookedTicketDetails().forEach(bookedTicketDetail -> emailService.sendSimpleMessage(customerAcc.getEmail(), bookedTicketDetail));
        }
    }
}
