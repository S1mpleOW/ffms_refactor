package s1mple.dlowji.ffms_refactor.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import s1mple.dlowji.ffms_refactor.controllers.BookedTicketController;
import s1mple.dlowji.ffms_refactor.entities.BookedTicket;
import s1mple.dlowji.ffms_refactor.entities.BookedTicketDetail;
import s1mple.dlowji.ffms_refactor.entities.FootballField;
import s1mple.dlowji.ffms_refactor.services.EmailService;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Service("EmailService")
public class EmailServiceImpl implements EmailService {

    private static final String NOREPLY_ADDRESS = "dev.loivo2k2@gmail.com";

    @Autowired
    private JavaMailSender emailSender;

    @Override
    public void sendSimpleMessage(String to, BookedTicketDetail bookedTicketDetail) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(NOREPLY_ADDRESS);
            message.setTo(to);
            FootballField field = bookedTicketDetail.getFootballField();
            ZonedDateTime date = bookedTicketDetail.getOrderDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, MMMM d");
            String formattedDate = date.format(formatter);

            String daySuffix;
            int dayOfMonth = date.getDayOfMonth();
            if (dayOfMonth >= 11 && dayOfMonth <= 13) {
                daySuffix = "th";
            } else if (dayOfMonth % 10 == 1) {
                daySuffix = "st";
            } else if (dayOfMonth % 10 == 2) {
                daySuffix = "nd";
            } else if (dayOfMonth % 10 == 3) {
                daySuffix = "rd";
            } else {
                daySuffix = "th";
            }

            formattedDate += dayOfMonth + daySuffix;
            message.setSubject("Upcoming Rental: " + field.getName() + " - " + formattedDate + " at " + bookedTicketDetail.getStartTime());
            String customerName = bookedTicketDetail.getBookedTicket().getCustomer().getAccount().getFullName();

            message.setText("Dear " + customerName + ",\n" +
                    "\n" +
                    "This is a friendly reminder that you have a rental scheduled for the " + field.getName()+ " on " + bookedTicketDetail.getOrderDate() +" from " + bookedTicketDetail.getStartTime() + " to "+ bookedTicketDetail.getEndTime()+". Please arrive at least 15 minutes early to ensure that you have enough time to check in and prepare for your rental.\n" +
                    "\n" +
                    "The football field is located at " + field.getFieldGroup().getAddress() +" , and there is ample parking available on site. Please make sure to bring your own equipment, such as cleats and shin guards, as they will not be provided.\n" +
                    "\n" +
                    "We hope you enjoy your rental experience at the Paimon Football Field Management Platform!\n" +
                    "\n" +
                    "Best regards,\n" +
                    "\n" +
                    "[Paimon Football Field Management Platform]");

            emailSender.send(message);
        } catch (MailException exception) {
            exception.printStackTrace();
        }
    }
}
