package s1mple.dlowji.ffms_refactor.services;

import s1mple.dlowji.ffms_refactor.entities.BookedTicketDetail;

public interface EmailService {
    void sendSimpleMessage(String to,
                           BookedTicketDetail bookedTicketDetail);
}
