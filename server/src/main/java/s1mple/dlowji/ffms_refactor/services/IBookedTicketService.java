package s1mple.dlowji.ffms_refactor.services;

import s1mple.dlowji.ffms_refactor.entities.BookedTicket;
import s1mple.dlowji.ffms_refactor.entities.BookedTicketDetail;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface IBookedTicketService {
	BookedTicket save(BookedTicket bookedTicket);

	BookedTicketDetail save(BookedTicketDetail bookedTicketDetail);

	List<BookedTicketDetail> getPlayedBookedTicketDetailByDate(Long accountId,
																														 LocalDateTime datetimeNow);

	List<BookedTicketDetail> getPlayingBookedTicketDetailByDate(LocalDateTime dateTime);

	List<BookedTicketDetail> getWillPlayBookedTicketDetailByDate(Long accountId,
																															 LocalDateTime datetimeNow);

	List<BookedTicketDetail> getBookedTicketDetailByWeek(LocalDateTime firstDay, LocalDateTime lastDay, Long field_id);

	List<BookedTicket> getBookedTicketByQuarter(int year, int quarter);

	List<BookedTicket> findBookedTicketByAccountId(Long accountId);

	int getBookedPriceByMonth(int month, int year);

	int getBookedPriceByDay(int day, int month, int year);
}
