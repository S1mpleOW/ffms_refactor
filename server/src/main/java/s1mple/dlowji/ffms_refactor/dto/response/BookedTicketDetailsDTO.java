package s1mple.dlowji.ffms_refactor.dto.response;

import lombok.*;
import s1mple.dlowji.ffms_refactor.entities.BookedTicket;
import s1mple.dlowji.ffms_refactor.entities.FootballField;
import s1mple.dlowji.ffms_refactor.entities.enums.FieldType;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookedTicketDetailsDTO {
	Long fieldId;
	LocalDateTime startTime;
	LocalDateTime endTime;
	private String name;
	private int totalPrice;
	private FieldType type;
	private double price;
}
