import React from 'react';
import Header from '../components/header/Header';
import TableBookedField from '../components/table/TableBookedField';
import { useState } from 'react';
import { BOOKED_FIELD } from '../utils/constant';
import { useAuth } from '../components/store/useAuth';
import { useEffect } from 'react';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import DateFnsUtils from '@date-io/date-fns';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import TextField from '@mui/material/TextField';

const bookedTableHead = ['', 'Field name', 'Start at', 'action'];
const renderHead = (item, index) => <th key={index}>{item}</th>;
const Booked = () => {
	const [bookedFields, setBookedFields] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [datetime, setDatetime] = useState(new Date().toISOString());
	const [filter, setFilter] = useState('will-play');
	const user = useAuth((store) => store.user);
	const handleFetchData = async () => {
		setIsLoading(true);
		const res = await fetch(`${BOOKED_FIELD}/${filter}?date=${datetime}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${user.token}`,
			},
		});
		setIsLoading(false);
		const data = await res.json();
		if (data?.status === 200) {
			setBookedFields(data?.fieldList);
		}
	};
	const handleChange = (event) => {
		setFilter(event.target.value);
	};
	useEffect(() => {
		handleFetchData();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [user.token, datetime, filter]);
	return (
		<div>
			<div className="header">
				<Header title="Booked fields"></Header>
				<div className="flex items-center gap-3">
					<FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
						<InputLabel>Filter by</InputLabel>
						<Select
							labelId="demo-simple-select-label"
							id="demo-simple-select"
							value={filter}
							label="Filter by"
							onChange={handleChange}
						>
							<MenuItem value={'will-play'}>Will play</MenuItem>
							<MenuItem value={'played'}>Played</MenuItem>
						</Select>
					</FormControl>
					<FormControl variant="outlined">
						<LocalizationProvider dateAdapter={DateFnsUtils}>
							<DatePicker
								label="Start at"
								inputVariant="outlined"
								value={datetime}
								onChange={(newValue) => {
									setDatetime(new Date(newValue).toISOString());
								}}
								renderInput={(params) => <TextField {...params} />}
							></DatePicker>
						</LocalizationProvider>
					</FormControl>
				</div>
			</div>
			<div className="row">
				<div className="col-12">
					<div className="card">
						<div className="card__body">
							<TableBookedField
								limit="10"
								headData={bookedTableHead}
								renderHead={(item, index) => renderHead(item, index)}
								bodyData={bookedFields}
								isLoading={isLoading}
								// handleRemove={handleRemove}
							></TableBookedField>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Booked;
