import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/images/football.png';
import { Button } from '@mui/material';
const Home = () => {
	return (
		<>
			<nav className="h-[84px]">
				<div className="flex justify-between items-center bg-[#FAFAFB] px-5 py-3">
					<Link to="/home" className="h-[50px]">
						<img src={logo} alt="tdtu_logo" className="object-cover w-full h-full" />
					</Link>
					<div className="flex items-center gap-3">
						<Button variant="outlined" color="success">
							<Link to="/login" className="no-underline">
								<span className="text-success">Login</span>
							</Link>
						</Button>
						<Button variant="contained" color="success">
							<Link to="/register" className="no-underline">
								<span className="text-white">Register</span>
							</Link>
						</Button>
					</div>
				</div>
			</nav>
			<main>
				<div className="h-[890px] pb-10 mx-auto bg-center bg-cover container-fluid bg-home">
					<div className="flex items-center justify-center">
						<div className="flex flex-col items-center justify-center gap-1 mt-5 text-slate-700">
							<h1 className="text-4xl font-bold">Welcome</h1>
							<p className="text-lg">Find a football field for rent in TDTU</p>
							<Button color="success" variant="contained" className="mt-5">
								<Link to="/login" className="text-white no-underline">
									Find a field
								</Link>
							</Button>
						</div>
					</div>
				</div>
			</main>
		</>
	);
};

export default Home;
