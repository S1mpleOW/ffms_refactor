import React, { useEffect } from 'react';
import { useAuth } from '../../components/store/useAuth';
import { useNavigate } from 'react-router-dom';

const Logout = () => {
	const setUser = useAuth((state) => state.setUser);
	const navigate = useNavigate();

	useEffect(() => {
		console.log('Logout');
		setUser(null);
		localStorage.removeItem('user');
		navigate('/home');
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return <div>Logout successfully</div>;
};

export default Logout;
