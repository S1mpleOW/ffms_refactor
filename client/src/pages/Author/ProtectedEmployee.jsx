import React from 'react';
import { useAuth } from '../../components/store/useAuth';
import { Outlet } from 'react-router-dom';

const ProtectedEmployee = () => {
	const user = useAuth((state) => state.user);

	if (!user?.roles?.includes('ADMIN') && !user?.roles?.includes('EMPLOYEE')) {
		return (
			<div className="flex flex-col justify-center items-center bg-[#FAFAFB] mt-10">
				<h1>You are not allowed to access this page</h1>
			</div>
		);
	}

	return <Outlet />;
};

export default ProtectedEmployee;
