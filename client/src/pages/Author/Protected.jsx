import { Navigate, Outlet } from 'react-router-dom';
import { useAuth } from '../../components/store/useAuth';
const Protected = () => {
	const user = useAuth((state) => state.user);
	const roles = user?.roles;
	if (!user || !roles?.length) {
		return <Navigate to="/home" />;
	}

	return <Outlet />;
};
export default Protected;
