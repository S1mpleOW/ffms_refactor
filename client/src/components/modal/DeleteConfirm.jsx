import React from 'react';
import { Button, Modal } from 'react-bootstrap';

const DeleteConfirm = ({ show, handleClose, handleDelete }) => {
	return (
		<Modal show={show} onHide={handleClose}>
			<Modal.Header closeButton>
				<Modal.Title>Are you sure?</Modal.Title>
			</Modal.Header>
			<Modal.Body>This action cannot be undone. Are you sure?</Modal.Body>
			<Modal.Footer>
				<Button variant="secondary" onClick={handleClose}>
					Close
				</Button>
				<Button variant="primary" onClick={handleDelete}>
					Yes, do it!
				</Button>
			</Modal.Footer>
		</Modal>
	);
};

export default DeleteConfirm;
