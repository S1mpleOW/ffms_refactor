import create from 'zustand';

export const useAuth = create((set) => {
	return {
		user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null,
		setUser: (user) => set({ user }),
	};
});
