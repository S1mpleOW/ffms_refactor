import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import UpdateButton from '../button/UpdateButton';
import Loading from '../loading/Loading';
import TooltipCustom from '../tooltip/TooltipCustom';
import { useAuth } from '../store/useAuth';
import { Link } from 'react-router-dom';
import DeleteButton from '../button/DeleteButton';
import ProfileButton from '../button/ProfileButton';

let pages = 1;

let range = [];
const TableBookedField = (props) => {
	const [dataShow, setDataShow] = useState(
		props.limit && props.bodyData ? props?.bodyData.slice(0, Number(props.limit)) : props?.bodyData
	);
	const [currPage, setCurrPage] = useState(0);
	const user = useAuth((store) => store.user);
	const isAdmin = user?.roles?.includes('ADMIN');

	if (props.limit !== undefined && props.bodyData !== undefined) {
		let page = Math.floor(props.bodyData.length / Number(props.limit));
		pages = props.bodyData.length % Number(props.limit) === 0 ? page : page + 1;
		range = [...Array(pages).keys()];
	}

	useEffect(() => {
		setDataShow(
			props.limit && props.bodyData
				? props?.bodyData.slice(0, Number(props.limit))
				: props?.bodyData
		);
	}, [props.bodyData, props.limit]);

	const selectPage = (page) => {
		const start = Number(props.limit) * page;
		const end = start + Number(props.limit);
		setDataShow(props.bodyData.slice(start, end));
		setCurrPage(page);
	};

	return (
		<div>
			<div className="table-wrapper">
				{props.bodyData && props.bodyData.length === 0 ? (
					<div className="flex justify-center flex-col items-center">
						<h2 className=" text-lg font-bold">You dont have booked matches</h2>
						<Link to="/fields" className="no-underline text-info hover:opacity-80">
							Click here to book a match now
						</Link>
					</div>
				) : (
					<table>
						{props.headData && props.renderHead ? (
							<thead>
								<tr>{props.headData.map((item, index) => props.renderHead(item, index))}</tr>
							</thead>
						) : null}
						<tbody>
							{props.isLoading ? (
								<tr className="">
									<td colSpan="6" className="text-center">
										<Loading />
									</td>
								</tr>
							) : props.bodyData && props.bodyData.length > 0 ? (
								dataShow.map((item, index) => {
									return (
										<tr key={index}>
											<td>{index + 1}</td>
											<td>{item.name}</td>
											<td>{new Date(item.startTime || Date.now()).toUTCString()}</td>
											<td className="flex items-center gap-2">
												<TooltipCustom content={'View detail'} placement={'top-end'} arrow>
													<ProfileButton to={`/fields/${item?.fieldId}`}></ProfileButton>
												</TooltipCustom>
											</td>
										</tr>
									);
								})
							) : null}
						</tbody>
					</table>
				)}
			</div>
			{pages > 1 ? (
				<div className="table__pagination">
					{range.map((item, index) => (
						<div
							key={index}
							className={`table__pagination-item ${currPage === index ? 'active' : ''}`}
							onClick={() => selectPage(index)}
						>
							{item + 1}
						</div>
					))}
				</div>
			) : null}
		</div>
	);
};

export default TableBookedField;
