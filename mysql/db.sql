CREATE DATABASE  IF NOT EXISTS `ffms_refactor_10_marks` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ffms_refactor_10_marks`;
-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ffms_refactor_10_marks
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `DOB` datetime DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `FULL_NAME` varchar(255) DEFAULT NULL,
  `IS_LOCKED` tinyint(1) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `PHONE` varchar(255) DEFAULT NULL,
  `SEX` varchar(255) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'2023-04-24 15:50:11','170/9 Nguyen Xi','2002-09-04 00:00:00','dungdqch@gmail.com','s1mple',0,'$2a$10$D189kJ5/qF1Ijn3dwG8e3.Pbv5qkKOv4qUeJHP9TIrCw7oACRO4aW','0325252522','MALE','2023-04-24 15:50:11','s1mple'),(2,'2023-04-24 20:27:03','123123','2002-02-20 00:00:00','dungdq22@gmail.com','s1mple2',0,'$2a$10$HnNCx5jHY...ss6tpaW/JOfdEp.Sy5WfOODya94Ekgh2N3n9XgZLW','0326084783','MALE','2023-04-25 21:12:55','s1mple2'),(3,'2023-04-25 12:44:43','360 pham van dong','2002-01-26 00:00:00','ka@gmail.com','ka',0,'$2a$10$hISG.w1k2n1YkBDMRO05DuzD4i5C/haBTnhx8mnB6W0rLgFjg04hy','0326094026','FEMALE','2023-04-25 21:16:05','ka123');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_role`
--

DROP TABLE IF EXISTS `account_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_role` (
  `ACCOUNT_ID` bigint NOT NULL,
  `ROLE_ID` bigint NOT NULL,
  PRIMARY KEY (`ACCOUNT_ID`,`ROLE_ID`),
  KEY `FK408ri96h727boiesrwk79jr8h` (`ROLE_ID`),
  CONSTRAINT `FK408ri96h727boiesrwk79jr8h` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`),
  CONSTRAINT `FKa9w3ftwoblt2hyu32fnhb3q2m` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_role`
--

LOCK TABLES `account_role` WRITE;
/*!40000 ALTER TABLE `account_role` DISABLE KEYS */;
INSERT INTO `account_role` VALUES (1,1),(1,2),(3,2),(1,3),(2,3),(3,3);
/*!40000 ALTER TABLE `account_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booked_ticket`
--

DROP TABLE IF EXISTS `booked_ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booked_ticket` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  `PAYMENT_STATUS` varchar(255) DEFAULT NULL,
  `TOTAL_PRICE` int DEFAULT NULL,
  `CUSTOMER_ID` bigint DEFAULT NULL,
  `EMPLOYEE_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKd4s1xqf1xuee3tg0ratuqw0ul` (`CUSTOMER_ID`),
  KEY `FK6injcb74hn2kk542no7pk36n0` (`EMPLOYEE_ID`),
  CONSTRAINT `FK6injcb74hn2kk542no7pk36n0` FOREIGN KEY (`EMPLOYEE_ID`) REFERENCES `employee` (`ID`),
  CONSTRAINT `FKd4s1xqf1xuee3tg0ratuqw0ul` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `customer` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booked_ticket`
--

LOCK TABLES `booked_ticket` WRITE;
/*!40000 ALTER TABLE `booked_ticket` DISABLE KEYS */;
INSERT INTO `booked_ticket` VALUES (1,'2023-04-25 00:10:06',NULL,'PROCESSING',180000,1,NULL),(2,'2023-04-25 11:49:14',NULL,'PROCESSING',150000,1,NULL),(3,'2023-04-25 11:53:01',NULL,'PROCESSING',150000,1,NULL),(4,'2023-04-25 12:43:46',NULL,'PROCESSING',150000,1,NULL),(5,'2023-04-25 13:17:45',NULL,'PROCESSING',150000,NULL,1),(6,'2023-04-25 13:18:02',NULL,'PROCESSING',150000,1,1),(7,'2023-04-26 20:57:12',NULL,'PROCESSING',180000,1,NULL);
/*!40000 ALTER TABLE `booked_ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booked_ticket_detail`
--

DROP TABLE IF EXISTS `booked_ticket_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booked_ticket_detail` (
  `BOOKED_TICKET_ID` bigint NOT NULL,
  `FOOTBALL_FIELD_ID` bigint NOT NULL,
  `DEPOSIT` int DEFAULT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `IS_CANCELED` tinyint(1) DEFAULT '0',
  `ORDER_DATE` datetime DEFAULT NULL,
  `START_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`BOOKED_TICKET_ID`,`FOOTBALL_FIELD_ID`),
  KEY `FKill2ctfda1hr6oll9tg8n3e2y` (`FOOTBALL_FIELD_ID`),
  CONSTRAINT `FKf1wop1pxes9hr2etd62t6ije3` FOREIGN KEY (`BOOKED_TICKET_ID`) REFERENCES `booked_ticket` (`ID`),
  CONSTRAINT `FKill2ctfda1hr6oll9tg8n3e2y` FOREIGN KEY (`FOOTBALL_FIELD_ID`) REFERENCES `football_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booked_ticket_detail`
--

LOCK TABLES `booked_ticket_detail` WRITE;
/*!40000 ALTER TABLE `booked_ticket_detail` DISABLE KEYS */;
INSERT INTO `booked_ticket_detail` VALUES (1,2,0,'2023-04-25 07:00:00',0,'2023-04-25 00:10:06','2023-04-25 06:00:00'),(2,2,0,'2023-04-26 11:00:00',0,'2023-04-25 11:49:14','2023-04-26 10:00:00'),(3,2,0,'2023-04-27 11:00:00',1,'2023-04-25 11:53:01','2023-04-27 10:00:00'),(4,2,0,'2023-04-27 11:00:00',0,'2023-04-25 12:43:46','2023-04-27 10:00:00'),(5,2,0,'2023-04-27 12:00:00',0,'2023-04-25 13:17:45','2023-04-27 11:00:00'),(6,2,0,'2023-04-27 13:00:00',0,'2023-04-25 13:18:02','2023-04-27 12:00:00'),(7,1,0,'2023-04-27 21:00:00',0,'2023-04-26 20:57:13','2023-04-27 20:00:00');
/*!40000 ALTER TABLE `booked_ticket_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `REWARD_POINT` bigint DEFAULT '0',
  `UPDATED_AT` datetime DEFAULT NULL,
  `ACCOUNT_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK192vqq0i3q2esrhd3mivortnl` (`ACCOUNT_ID`),
  CONSTRAINT `FK192vqq0i3q2esrhd3mivortnl` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'2023-04-24 20:27:03',50,'2023-04-26 20:57:12',2);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `IDENTITY_CARD` varchar(255) DEFAULT NULL,
  `SALARY` double DEFAULT '0',
  `UPDATED_AT` datetime DEFAULT NULL,
  `ACCOUNT_ID` bigint DEFAULT NULL,
  `FIELD_GROUP_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKe1p2i0nh8jge7utgha0nftdvs` (`ACCOUNT_ID`),
  KEY `FKg1i27v2rek00ya2nud3a2o790` (`FIELD_GROUP_ID`),
  CONSTRAINT `FKe1p2i0nh8jge7utgha0nftdvs` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKg1i27v2rek00ya2nud3a2o790` FOREIGN KEY (`FIELD_GROUP_ID`) REFERENCES `field_group` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'2023-04-25 12:44:43','Shift 3','1900805822',1000000,'2023-04-25 12:44:43',3,1);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_group`
--

DROP TABLE IF EXISTS `field_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `field_group` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `CLOSE_TIME` time DEFAULT NULL,
  `FIELD_FEE_WEIGHT` double DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `OPEN_TIME` time DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_group`
--

LOCK TABLES `field_group` WRITE;
/*!40000 ALTER TABLE `field_group` DISABLE KEYS */;
INSERT INTO `field_group` VALUES (1,'2022-11-09 21:07:46','19 Đ. Nguyễn Hữu Thọ, Tân Hưng, Quận 7, Thành phố Hồ Chí Minh','23:59:59',1,'Tôn đức thắng university','05:00:00');
/*!40000 ALTER TABLE `field_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `football_field`
--

DROP TABLE IF EXISTS `football_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `football_field` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `IMAGE` longtext,
  `NAME` varchar(255) DEFAULT NULL,
  `PRICE` double DEFAULT '0',
  `TYPE` varchar(255) DEFAULT NULL,
  `FIELD_GROUP_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK1j9hv6b6e7kmoma50mt0mk3dh` (`FIELD_GROUP_ID`),
  CONSTRAINT `FK1j9hv6b6e7kmoma50mt0mk3dh` FOREIGN KEY (`FIELD_GROUP_ID`) REFERENCES `field_group` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `football_field`
--

LOCK TABLES `football_field` WRITE;
/*!40000 ALTER TABLE `football_field` DISABLE KEYS */;
INSERT INTO `football_field` VALUES (1,'2022-11-09 21:08:31','https://i.ibb.co/yYT3V2P/field5-1.png','TDTU sân 1',150000,'FIELD_5',1),(2,'2022-11-09 21:08:52','https://i.ibb.co/SKFs6CJ/field5-2.jpg','TDTU sân 2',150000,'FIELD_5',1),(3,'2022-11-09 21:13:01','https://i.ibb.co/3shFfN5/field7-1.png','TDTU Sân 3',200000,'FIELD_7',1),(4,'2022-11-09 21:13:22','https://i.ibb.co/8gdnpw2/field7-2.jpg','TDTU sân 4',200000,'FIELD_7',1),(5,'2022-11-09 21:13:49','https://i.ibb.co/JtkxcZd/field11-1.jpg','TDTU Sân 5',300000,'FIELD_11',1),(6,'2022-11-09 21:14:14','https://i.ibb.co/L6xNMzd/filed11-2.jpg','TDTU sân 6',300000,'FIELD_11',1),(7,'2022-11-12 10:52:04','https://i.ibb.co/1MTVh2J/filed11-2.jpg','TDTU Sân 7',150000,'FIELD_5',1);
/*!40000 ALTER TABLE `football_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_receipt`
--

DROP TABLE IF EXISTS `import_receipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `import_receipt` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  `PAYMENT_STATUS` varchar(255) DEFAULT NULL,
  `TOTAL_PRICE` int DEFAULT NULL,
  `EMPLOYEE_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKf8not0qyq85olpejisq8d85k9` (`EMPLOYEE_ID`),
  CONSTRAINT `FKf8not0qyq85olpejisq8d85k9` FOREIGN KEY (`EMPLOYEE_ID`) REFERENCES `employee` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_receipt`
--

LOCK TABLES `import_receipt` WRITE;
/*!40000 ALTER TABLE `import_receipt` DISABLE KEYS */;
INSERT INTO `import_receipt` VALUES (1,'2023-04-24 16:15:43','GUI NGAY','PROCESSING',50000,NULL),(2,'2023-04-24 16:16:00','Gui le di','PROCESSING',100000,NULL),(3,'2023-04-24 16:16:21','gui le gium cai','PROCESSING',100000,NULL),(10,'2023-04-24 17:37:03','','PROCESSING',1000000,NULL),(11,'2023-04-25 13:14:47','','PROCESSING',50000,1);
/*!40000 ALTER TABLE `import_receipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_receipt_detail`
--

DROP TABLE IF EXISTS `import_receipt_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `import_receipt_detail` (
  `IMPORT_RECEIPT_ID` bigint NOT NULL,
  `ITEM_ID` bigint NOT NULL,
  `DELIVERY_DATE` datetime DEFAULT NULL,
  `ORDER_DATE` datetime DEFAULT NULL,
  `QUANTITY` int DEFAULT NULL,
  PRIMARY KEY (`IMPORT_RECEIPT_ID`,`ITEM_ID`),
  KEY `FK4ajleboatwr8jyup2pkkyfghu` (`ITEM_ID`),
  CONSTRAINT `FK3v1pu4ni8n57ult1qoe61os0p` FOREIGN KEY (`IMPORT_RECEIPT_ID`) REFERENCES `import_receipt` (`ID`),
  CONSTRAINT `FK4ajleboatwr8jyup2pkkyfghu` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_receipt_detail`
--

LOCK TABLES `import_receipt_detail` WRITE;
/*!40000 ALTER TABLE `import_receipt_detail` DISABLE KEYS */;
INSERT INTO `import_receipt_detail` VALUES (1,1,'2023-04-24 16:15:00','2023-04-24 16:15:43',5),(2,1,'2023-04-24 16:15:00','2023-04-24 16:16:00',10),(3,1,'2023-04-24 16:16:00','2023-04-24 16:16:21',10),(10,10,'2023-04-24 00:00:00','2023-04-24 17:37:03',10),(11,1,'2023-04-25 13:14:00','2023-04-25 13:14:47',5);
/*!40000 ALTER TABLE `import_receipt_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `IMAGE` longtext,
  `IMPORT_PRICE` int DEFAULT NULL,
  `CATEGORY` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  `QUANTITY` int DEFAULT NULL,
  `SELL_PRICE` int DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `UNIT` varchar(255) DEFAULT NULL,
  `SUPPLIER_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK5vdfn4b0b579a13i0jsihhdrs` (`SUPPLIER_ID`),
  CONSTRAINT `FK5vdfn4b0b579a13i0jsihhdrs` FOREIGN KEY (`SUPPLIER_ID`) REFERENCES `supplier` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,'2022-11-09 20:33:29','https://i.ibb.co/85mYCYF/COCA.jpg',10000,'DRINK','Coca','',33,15000,'AVAILABLE','Chai',1),(2,'2022-11-09 20:35:47','https://i.ibb.co/Ks39y2d/PEPSI.jpg',10000,'DRINK','Pepsi','',24,15000,'AVAILABLE','Lon',2),(3,'2022-11-09 20:36:49','https://i.ibb.co/cxhsBWJ/REVIVE.jpg',12000,'DRINK','Revive','',30,15000,'AVAILABLE','Chai',1),(4,'2022-11-09 20:37:43','https://i.ibb.co/yW00PQK/RED-BULL.jpg',15000,'DRINK','Red bull','',14,15000,'AVAILABLE','Lon',1),(5,'2022-11-09 20:39:18','https://i.ibb.co/rZDCVtq/MOUNTAIN-DEW.jpg',12000,'DRINK','Mountain dew','',27,15000,'AVAILABLE','Chai',2),(6,'2022-11-09 20:40:20','https://i.ibb.co/023njDR/AO-PITCH-XANH.jpg',100000,'PITCH','Pitch xanh','',48,150000,'AVAILABLE','Cái',3),(7,'2022-11-09 20:41:21','https://i.ibb.co/YfHb0Rg/AO-PITCH-CAM.jpg',100000,'PITCH','Pitch cam','',42,150000,'AVAILABLE','Cái',3),(8,'2022-11-09 20:42:00','https://i.ibb.co/K9ZM8jZ/AO-PITCH-VANG.jpg',100000,'PITCH','Pitch vàng','',20,150000,'AVAILABLE','Cái',3),(9,'2022-11-09 20:47:26','https://i.ibb.co/k8ySvHZ/PEPSI-CHANH.jpg',10000,'DRINK','Pepsi vị chanh','',9,10000,'AVAILABLE','Cái',1),(10,'2023-04-24 17:37:03','https://i.ibb.co/Hqnf9PJ/Ao-luoi-xanh-duong.jpg',100000,'DRINK','Pitch xanh dương','',8,150000,'AVAILABLE','Cái',3);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_epk9im9l9q67xmwi4hbed25do` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ADMIN'),(2,'EMPLOYEE'),(3,'USER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'2023-04-24 16:10:58','Bán nước'),(2,'2023-04-24 16:57:43','Bán quần áo');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_item`
--

DROP TABLE IF EXISTS `service_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_item` (
  `SERVICE_ID` bigint NOT NULL,
  `ITEM_ID` bigint NOT NULL,
  PRIMARY KEY (`SERVICE_ID`,`ITEM_ID`),
  KEY `FK55sogfs0l9oe0f3krhy2wy59r` (`ITEM_ID`),
  CONSTRAINT `FK55sogfs0l9oe0f3krhy2wy59r` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FKg4nhkwdmn3d7wh6kgkvedtohb` FOREIGN KEY (`SERVICE_ID`) REFERENCES `service` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_item`
--

LOCK TABLES `service_item` WRITE;
/*!40000 ALTER TABLE `service_item` DISABLE KEYS */;
INSERT INTO `service_item` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(2,6),(2,7),(2,8),(1,9),(2,10);
/*!40000 ALTER TABLE `service_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_receipt`
--

DROP TABLE IF EXISTS `service_receipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_receipt` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  `PAYMENT_STATUS` varchar(255) DEFAULT NULL,
  `TOTAL_PRICE` int DEFAULT NULL,
  `EMPLOYEE_ID` bigint DEFAULT NULL,
  `USER_ID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKe865gxhnxwvxdod7ndnt5640n` (`EMPLOYEE_ID`),
  KEY `FKn1nkwvm4xvct0drul7229an8g` (`USER_ID`),
  CONSTRAINT `FKe865gxhnxwvxdod7ndnt5640n` FOREIGN KEY (`EMPLOYEE_ID`) REFERENCES `employee` (`ID`),
  CONSTRAINT `FKn1nkwvm4xvct0drul7229an8g` FOREIGN KEY (`USER_ID`) REFERENCES `customer` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_receipt`
--

LOCK TABLES `service_receipt` WRITE;
/*!40000 ALTER TABLE `service_receipt` DISABLE KEYS */;
INSERT INTO `service_receipt` VALUES (1,'2023-04-24 17:57:35',NULL,'PROCESSING',150000,NULL,NULL),(2,'2023-04-24 17:59:16',NULL,'PROCESSING',300000,NULL,NULL),(3,'2023-04-24 17:59:57',NULL,'PROCESSING',90000,NULL,NULL),(4,'2023-04-24 18:00:11',NULL,'PROCESSING',450000,NULL,NULL),(5,'2023-04-24 21:05:05',NULL,'PROCESSING',15000,NULL,NULL),(6,'2023-04-24 21:07:05',NULL,'PROCESSING',15000,NULL,NULL),(7,'2023-04-24 21:33:12',NULL,'PROCESSING',15000,NULL,NULL),(8,'2023-04-24 21:36:48',NULL,'PROCESSING',15000,NULL,NULL),(9,'2023-04-24 21:43:23',NULL,'PROCESSING',30000,NULL,NULL),(10,'2023-04-24 22:02:52',NULL,'PROCESSING',15000,NULL,NULL),(11,'2023-04-24 22:14:22',NULL,'PROCESSING',15000,NULL,NULL),(12,'2023-04-24 22:32:45',NULL,'PROCESSING',15000,NULL,NULL),(13,'2023-04-24 22:34:50',NULL,'PROCESSING',15000,NULL,1),(14,'2023-04-25 00:08:49',NULL,'PROCESSING',450000,NULL,1),(15,'2023-04-25 00:09:33',NULL,'PROCESSING',150000,NULL,NULL),(16,'2023-04-25 12:40:15',NULL,'PROCESSING',150000,NULL,1),(17,'2023-04-25 12:42:49',NULL,'PROCESSING',150000,NULL,NULL),(18,'2023-04-25 13:13:53',NULL,'PROCESSING',25000,1,NULL),(19,'2023-04-25 13:14:11',NULL,'PROCESSING',30000,1,1);
/*!40000 ALTER TABLE `service_receipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_receipt_detail`
--

DROP TABLE IF EXISTS `service_receipt_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_receipt_detail` (
  `ITEM_ID` bigint NOT NULL,
  `SERVICE_RECEIPT_ID` bigint NOT NULL,
  `ORDER_DATE` datetime DEFAULT NULL,
  `QUANTITY` int DEFAULT NULL,
  PRIMARY KEY (`ITEM_ID`,`SERVICE_RECEIPT_ID`),
  KEY `FKd7ttclym3ljle11w9bjk1e8v6` (`SERVICE_RECEIPT_ID`),
  CONSTRAINT `FKbrl6bvwawn8mybs0oxmde12fx` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FKd7ttclym3ljle11w9bjk1e8v6` FOREIGN KEY (`SERVICE_RECEIPT_ID`) REFERENCES `service_receipt` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_receipt_detail`
--

LOCK TABLES `service_receipt_detail` WRITE;
/*!40000 ALTER TABLE `service_receipt_detail` DISABLE KEYS */;
INSERT INTO `service_receipt_detail` VALUES (1,3,'2023-04-24 17:59:57',3),(1,5,'2023-04-24 21:05:05',1),(1,10,'2023-04-24 22:02:52',1),(1,19,'2023-04-25 13:14:11',2),(2,9,'2023-04-24 21:43:23',2),(2,13,'2023-04-24 22:34:50',1),(3,3,'2023-04-24 17:59:57',3),(3,6,'2023-04-24 21:07:05',1),(3,7,'2023-04-24 21:33:12',1),(4,18,'2023-04-25 13:13:53',1),(5,8,'2023-04-24 21:36:48',1),(5,11,'2023-04-24 22:14:22',1),(5,12,'2023-04-24 22:32:45',1),(6,1,'2023-04-24 17:57:35',1),(6,2,'2023-04-24 17:59:16',1),(7,14,'2023-04-25 00:08:49',2),(7,15,'2023-04-25 00:09:33',1),(8,4,'2023-04-24 18:00:11',3),(8,14,'2023-04-25 00:08:49',1),(8,16,'2023-04-25 12:40:15',1),(9,18,'2023-04-25 13:13:53',1),(10,2,'2023-04-24 17:59:16',1),(10,17,'2023-04-25 12:42:49',1);
/*!40000 ALTER TABLE `service_receipt_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supplier` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `CREATED_AT` datetime DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  `PHONE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (1,'2022-11-09 20:29:45','485 Song Hành XL Hà Nội, Phường Linh Trung, Thủ Đức, Thành phố Hồ Chí Minh','Coca-Cola','Chuyên cung cấp nước','02838961000'),(2,'2022-11-09 20:30:47','88 Đồng Khởi, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh','Suntory PepsiCo Việt Nam','','02866848422'),(3,'2022-11-09 20:31:35','67 Đ. Nguyễn Hồng Đào, Phường 14, Tân Bình, Thành phố Hồ Chí Minh 700000','KiwiSport','Chuyên lấy sỉ quần áo','0962471206');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-26 21:22:00
